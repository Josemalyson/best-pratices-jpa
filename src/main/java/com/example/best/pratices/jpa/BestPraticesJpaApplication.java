package com.example.best.pratices.jpa;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.best.pratices.jpa.model.Stundent;
import com.example.best.pratices.jpa.repository.StudentRepository;
import com.example.best.pratices.jpa.repository.SubjectMatterRepository;

@SpringBootApplication
public class BestPraticesJpaApplication {

	@Bean
	CommandLineRunner runner(StudentRepository studentRepository, SubjectMatterRepository subjectMatterRepository) {
		return student -> {
			mapeamentoManyToOneUnidirecional(studentRepository, subjectMatterRepository);
		};
	}

	public static void main(String[] args) {
		SpringApplication.run(BestPraticesJpaApplication.class, args);
	}

	private void mapeamentoManyToOneUnidirecional(StudentRepository studentRepository,
			SubjectMatterRepository subjectMatterRepository) {
		// CRIANDO UM ALUNO NAO DEPENDE DA DISCIPLINA
		Stundent newStudent = new Stundent();
		newStudent.setName("Jose");
		newStudent.setDtBirthDay(new Date());
		newStudent = studentRepository.save(newStudent);

	}
}
