package com.example.best.pratices.jpa.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TB_SUBJECT_MATTER")
public class SubjectMatter implements Serializable {

	private static final long serialVersionUID = 5878768427697126475L;

	@Id
	@Column(name = "ID_STUDENT", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "DS_NAME", length = 255, nullable = true)
	private String name;

	@Column(name = "DS_DESCRIPTION", length = 255)
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "FK_STUDENT")
	private Stundent stundent;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SubjectMatter other = (SubjectMatter) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "SubjectMatter [id=" + id + ", name=" + name + ", description=" + description + "]";
	}

	public Stundent getStundent() {
		return stundent;
	}

	public void setStundent(Stundent stundent) {
		this.stundent = stundent;
	}

}
