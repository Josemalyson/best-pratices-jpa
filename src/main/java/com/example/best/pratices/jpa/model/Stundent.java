package com.example.best.pratices.jpa.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TB_STUDENT")
public class Stundent implements Serializable {

	private static final long serialVersionUID = 1847248900954992938L;

	@Id
	@Column(name = "ID_STUDENT", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "DS_NAME", length = 255, nullable = true)
	private String name;

	@Temporal(value = TemporalType.DATE)
	@Column(name = "DT_BIRTHDAY", nullable = true)
	private Date dtBirthDay;

//	/**
//	 * {@link JoinColumn} Esta propiedade define a chave estrageira (FK) da tabela
//	 * {@link SubjectMatter}
//	 */
//
//	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
//	@JoinColumn(name = "FK_SUBJECT_MATTERS")
//	private List<SubjectMatter> subjectMatters = new ArrayList<>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtBirthDay() {
		return dtBirthDay;
	}

	public void setDtBirthDay(Date dtBirthDay) {
		this.dtBirthDay = dtBirthDay;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stundent other = (Stundent) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Stundent [id=" + id + ", name=" + name + ", dtBirthDay=" + dtBirthDay + "]";
	}

//	public List<SubjectMatter> getSubjectMatters() {
//		return subjectMatters;
//	}
//
//	public void setSubjectMatters(List<SubjectMatter> subjectMatters) {
//		this.subjectMatters = subjectMatters;
//	}

}
