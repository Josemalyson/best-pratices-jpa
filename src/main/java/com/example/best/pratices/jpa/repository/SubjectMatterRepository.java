package com.example.best.pratices.jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.best.pratices.jpa.model.SubjectMatter;

public interface SubjectMatterRepository extends JpaRepository<SubjectMatter, Long> {

}
