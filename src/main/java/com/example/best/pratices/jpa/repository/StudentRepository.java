package com.example.best.pratices.jpa.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.best.pratices.jpa.model.Stundent;

public interface StudentRepository extends CrudRepository<Stundent, Long> {

}
